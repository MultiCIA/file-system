/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import io.gitlab.multicia.graph.Element;
import io.gitlab.multicia.graph.ElementConstructor;
import io.gitlab.multicia.graph.ElementHasher;
import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.MatchingType;
import io.gitlab.multicia.graph.Node;
import io.gitlab.multicia.graph.Signature;
import io.gitlab.multicia.graph.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

public final class FileSystemHelper {
	/**
	 * Constructors for {@link FileSystemNode}s and {@link FileSystemGroup}.
	 */
	public static final @NotNull Map<@NotNull Signature, @NotNull ElementConstructor>
			FILE_SYSTEM_CONSTRUCTORS =
			Map.of(
					FileSystemGroup.SIGNATURE, FileSystemGroup::new,
					FileSystemNode.SIGNATURE, FileSystemNode::new,
					RootNode.SIGNATURE, RootNode::new,
					FileNode.SIGNATURE, FileNode::new,
					DirectoryNode.SIGNATURE, DirectoryNode::new,
					SymbolicLinkNode.SIGNATURE, SymbolicLinkNode::new
			);

	private static final @NotNull List<@NotNull FieldHasher<? super FileSystemNode>>
			FILE_SYSTEM_NODE_FIELD_HASHERS =
			List.of(
					Element.SIGNATURE_HASHER,
					TreeNode.PARENT_HASHER,
					FileSystemNode.NAME_HASHER
			);
	private static final @NotNull List<@NotNull FieldMatcher<? super FileSystemNode, ? super FileSystemNode>>
			FILE_SYSTEM_NODE_FIELD_MATCHERS =
			List.of(
					Element.SIGNATURE_MATCHER,
					TreeNode.PARENT_MATCHER,
					FileSystemNode.NAME_MATCHER
			);
	private static final @NotNull List<@NotNull FieldHasher<? super SymbolicLinkNode>>
			SYMBOLIC_LINK_NODE_FIELD_HASHERS =
			List.of(
					Element.SIGNATURE_HASHER,
					TreeNode.PARENT_HASHER,
					FileSystemNode.NAME_HASHER,
					SymbolicLinkNode.TARGET_HASHER
			);
	private static final @NotNull List<@NotNull FieldMatcher<? super SymbolicLinkNode, ? super SymbolicLinkNode>>
			SYMBOLIC_LINK_NODE_FIELD_MATCHERS =
			List.of(
					Element.SIGNATURE_MATCHER,
					TreeNode.PARENT_MATCHER,
					FileSystemNode.NAME_MATCHER,
					SymbolicLinkNode.TARGET_MATCHER
			);

	/**
	 * Create a set of {@link ElementHasher} for {@link FileSystemNode} type. These hashers calculate hash both the file
	 * name and the structure of {@link FileSystemNode}s.
	 *
	 * @param matchingType The matching type.
	 * @return A set of {@link ElementHasher}.
	 */
	public static @Unmodifiable @NotNull Set<@NotNull ElementHasher<?>> createIdenticalHasher(
			@NotNull MatchingType matchingType
	) {
		return Set.of(
				new ElementHasher<>(matchingType, RootNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_HASHERS)),
				new ElementHasher<>(matchingType, FileNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_HASHERS)),
				new ElementHasher<>(matchingType, DirectoryNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_HASHERS)),
				new ElementHasher<>(matchingType, SymbolicLinkNode.class,
						SYMBOLIC_LINK_NODE_FIELD_HASHERS)
		);
	}

	/**
	 * Create a set of {@link ElementMatcher} for {@link FileSystemNode} type. These matchers compare both the file name
	 * and the structure of {@link FileSystemNode}s, and only matched them if they are identical.
	 *
	 * @param matchingType The matching type.
	 * @return A set of {@link ElementMatcher}.
	 */
	public static @Unmodifiable @NotNull Set<@NotNull ElementMatcher<?, ?>> createIdenticalMatchers(
			@NotNull MatchingType matchingType
	) {
		return Set.of(
				new ElementMatcher<>(matchingType, RootNode.class, RootNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_MATCHERS)),
				new ElementMatcher<>(matchingType, FileNode.class, FileNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_MATCHERS)),
				new ElementMatcher<>(matchingType, DirectoryNode.class, DirectoryNode.class,
						List.copyOf(FILE_SYSTEM_NODE_FIELD_MATCHERS)),
				new ElementMatcher<>(matchingType, SymbolicLinkNode.class, SymbolicLinkNode.class,
						SYMBOLIC_LINK_NODE_FIELD_MATCHERS)
		);
	}


	private FileSystemHelper() {
	}


	private static @NotNull Deque<@NotNull FileSystemNode> createNodeStack(@NotNull FileSystemNode node) {
		final Deque<FileSystemNode> nodes = new ArrayDeque<>();
		while (true) {
			nodes.push(node);
			final Node parent = node.getParent();
			if (parent instanceof FileSystemNode fileSystemNode && !(parent instanceof RootNode)) {
				node = fileSystemNode;
			} else {
				break;
			}
		}
		return nodes;
	}

	/**
	 * Create absolute path {@link String} from a {@link FileSystemNode}. This is done by joining their name with
	 * {@code "/"} as a delimiter. Any {@link FileSystemNode} with a {@code null} name will be replaced with
	 * {@code "<unnamed>"}.
	 */
	public static @NotNull String toAbsolutePathString(@NotNull FileSystemNode node) {
		if (node instanceof RootNode) return "";
		final Deque<FileSystemNode> nodeStack = createNodeStack(node);
		final StringJoiner joiner = new StringJoiner("/");
		for (final FileSystemNode fileSystemNode : nodeStack) {
			final String name = fileSystemNode.getName();
			joiner.add(name != null ? name : "<unnamed>");
		}
		return joiner.toString();
	}

	/**
	 * Create absolute {@link Path} from a {@link FileSystemNode}. This is done by joining their name with {@code "/"}
	 * as a delimiter. Any {@link FileSystemNode} with a {@code null} name will be replaced with {@code "<unnamed>"}.
	 */
	public static @NotNull Path toPath(@NotNull FileSystemNode node) {
		final Path emptyPath = Path.of("");
		if (node instanceof RootNode) return emptyPath;
		final Deque<FileSystemNode> nodeStack = createNodeStack(node);
		Path path = emptyPath;
		for (final FileSystemNode fileSystemNode : nodeStack) {
			final String name = fileSystemNode.getName();
			path = path.resolve(name != null ? name : "<unnamed>");
		}
		return path;
	}

	/**
	 * Resolve a {@link FileSystemNode} by a relative {@link Path}. This {@link Path} is only used as a name element
	 * array.  If followLink is set to true, any matching {@link SymbolicLinkNode} will be followed, even when the
	 * target is unknown (in this case, this method return {@code null}).
	 */
	public static @Nullable FileSystemNode resolveNodeByRelativePath(@NotNull RootNode rootNode,
			@NotNull Path relativePath, boolean followLink) {
		FileSystemNode currentNode = rootNode;
		for (final Path elementPath : relativePath) {
			currentNode = resolveChildNodeByName(currentNode, elementPath.toString(), followLink);
			if (currentNode == null) return null;
		}
		return currentNode;
	}

	/**
	 * Resolve a child {@link FileSystemNode} by name. If {@code followLink} is set to {@code true}, matching
	 * {@link SymbolicLinkNode} will be followed, even when the target is unknown (in this case, this method return
	 * {@code null}).
	 */
	public static @Nullable FileSystemNode resolveChildNodeByName(@NotNull FileSystemNode parentNode,
			@NotNull String childName, boolean followLink) {
		for (final Node childNode : parentNode.getChildren()) {
			if (childNode instanceof FileSystemNode fileSystemNode
					&& childName.equals(fileSystemNode.getName())) {
				return followLink && fileSystemNode instanceof SymbolicLinkNode symbolicLinkNode
						? symbolicLinkNode.getTarget()
						: fileSystemNode;
			}
		}
		return null;
	}
}
