/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import io.gitlab.multicia.graph.Graph;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Pattern;

public final class FileSystemBuilder {
	private @Nullable Graph graph;
	private @Nullable Path rootPath;
	private @Nullable List<@NotNull Path> relativeIncludePaths;
	private @Nullable List<@NotNull Path> relativeExcludePaths;
	private @Nullable List<@NotNull Pattern> includePatterns;
	private @Nullable List<@NotNull Pattern> excludePatterns;
	private boolean skipError;

	
	public FileSystemBuilder() {
	}


	public @Nullable Graph getGraph() {
		return graph;
	}

	public @NotNull FileSystemBuilder setGraph(@Nullable Graph graph) {
		this.graph = graph;
		return this;
	}

	public @Nullable Path getRootPath() {
		return rootPath;
	}

	public @NotNull FileSystemBuilder setRootPath(@Nullable Path rootPath) {
		this.rootPath = rootPath;
		return this;
	}

	public @Nullable List<@NotNull Path> getRelativeIncludePaths() {
		return relativeIncludePaths;
	}

	public @NotNull FileSystemBuilder setRelativeIncludePaths(@Nullable List<@NotNull Path> relativeIncludePaths) {
		this.relativeIncludePaths = relativeIncludePaths;
		return this;
	}

	public @Nullable List<@NotNull Path> getRelativeExcludePaths() {
		return relativeExcludePaths;
	}

	public @NotNull FileSystemBuilder setRelativeExcludePaths(@Nullable List<@NotNull Path> relativeExcludePaths) {
		this.relativeExcludePaths = relativeExcludePaths;
		return this;
	}

	public @Nullable List<@NotNull Pattern> getIncludePatterns() {
		return includePatterns;
	}

	public @NotNull FileSystemBuilder setIncludePatterns(@Nullable List<@NotNull Pattern> includePatterns) {
		this.includePatterns = includePatterns;
		return this;
	}

	public @Nullable List<@NotNull Pattern> getExcludePatterns() {
		return excludePatterns;
	}

	public @NotNull FileSystemBuilder setExcludePatterns(@Nullable List<@NotNull Pattern> excludePatterns) {
		this.excludePatterns = excludePatterns;
		return this;
	}

	public boolean isSkipError() {
		return skipError;
	}

	public @NotNull FileSystemBuilder setSkipError(boolean skipError) {
		this.skipError = skipError;
		return this;
	}


	public @NotNull FileSystemGroup build() throws IOException {
		if (graph == null || rootPath == null) throw new IllegalStateException();
		final FileSystemGroup group = new FileSystemGroup();
		FileSystemVisitor.build(group, rootPath, relativeIncludePaths, relativeExcludePaths,
				includePatterns, excludePatterns, skipError);
		graph.add(group);
		return group;
	}
}
