/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

class FileSystemVisitor implements FileVisitor<Path> {
	private static final @NotNull Logger LOGGER = Logger.getLogger(FileSystemVisitor.class.getName());

	private final @NotNull FileSystemGroup group;
	private final @Nullable List<@NotNull Path> includePaths;
	private final @Nullable List<@NotNull Path> excludePaths;
	private final @Nullable List<@NotNull Pattern> includePatterns;
	private final @Nullable List<@NotNull Pattern> excludePatterns;
	private final boolean skipError;

	private final @NotNull Map<@NotNull Path, @NotNull FileSystemNode> nodeMap;
	private final @NotNull List<@NotNull Pair<@NotNull SymbolicLinkNode, @NotNull Path>> delayedLinks;


	private FileSystemVisitor(@NotNull FileSystemGroup group, @NotNull Path rootPath,
			@Nullable List<@NotNull Path> includePaths, @Nullable List<@NotNull Path> excludePaths,
			@Nullable List<@NotNull Pattern> includePatterns, @Nullable List<@NotNull Pattern> excludePatterns,
			boolean skipError) {
		this.group = group;
		this.includePaths = includePaths;
		this.excludePaths = excludePaths;
		this.includePatterns = includePatterns;
		this.excludePatterns = excludePatterns;
		this.skipError = skipError;

		this.nodeMap = new HashMap<>();
		this.delayedLinks = new ArrayList<>();

		final RootNode rootNode = new RootNode();
		group.add(rootNode);
		nodeMap.put(rootPath, rootNode);
	}

	private static @NotNull List<@NotNull Path> createRealPathsFromRelativePaths(@NotNull Path rootPath,
			@NotNull List<@NotNull Path> relativePaths) throws IOException {
		final List<Path> realPaths = new ArrayList<>(relativePaths.size());
		for (final Path relativePath : relativePaths) {
			final Path absolutePath = rootPath.resolve(relativePath);
			if (Files.exists(absolutePath, LinkOption.NOFOLLOW_LINKS)) {
				final Path realPath = absolutePath.toRealPath(LinkOption.NOFOLLOW_LINKS);
				if (realPath.startsWith(rootPath)) {
					realPaths.add(realPath);
				} else {
					LOGGER.info("Skipping path \"%s\": not exist inside root path!".formatted(realPath));
				}
			} else {
				LOGGER.info("Skipping path \"%s\": not exist!".formatted(absolutePath));
			}
		}
		return realPaths;
	}

	static void build(@NotNull FileSystemGroup group, @NotNull Path rootPath,
			@Nullable List<@NotNull Path> relativeIncludePaths, @Nullable List<@NotNull Path> relativeExcludePaths,
			@Nullable List<@NotNull Pattern> includePatterns, @Nullable List<@NotNull Pattern> excludePatterns,
			boolean skipError) throws IOException {
		final Path realRootPath = rootPath.toRealPath(LinkOption.NOFOLLOW_LINKS);
		final List<Path> realIncludePaths = relativeIncludePaths != null
				? createRealPathsFromRelativePaths(realRootPath, relativeIncludePaths)
				: null;
		final List<Path> realExcludePaths = relativeExcludePaths != null
				? createRealPathsFromRelativePaths(realRootPath, relativeExcludePaths)
				: null;
		final FileSystemVisitor visitor = new FileSystemVisitor(group, realRootPath, realIncludePaths, realExcludePaths,
				includePatterns, excludePatterns, skipError);
		Files.walkFileTree(realRootPath, visitor);
		visitor.finalizeResult();
	}


	private void finalizeResult() throws IOException {
		for (final Pair<SymbolicLinkNode, Path> pair : delayedLinks) {
			final SymbolicLinkNode linkNode = pair.one();
			final Path path = pair.two();
			final Path targetPath = Files.readSymbolicLink(path);
			final Path absolutePath = targetPath.isAbsolute()
					? targetPath.normalize()
					: path.resolveSibling(targetPath).normalize();
			final FileSystemNode targetNode = nodeMap.get(absolutePath);
			if (targetNode != null) {
				linkNode.setTarget(targetNode);
			} else {
				LOGGER.info("Skipping link target to \"%s\": not exist inside root path!".formatted(absolutePath));
			}
		}
	}


	private @NotNull DirectoryNode newDirectory(@NotNull Path directory) {
		final FileSystemNode parentNode = createDirectory(directory.getParent());
		final DirectoryNode directoryNode = new DirectoryNode();
		group.add(directoryNode);
		directoryNode.setName(directory.getFileName().toString());
		directoryNode.setParent(parentNode);
		return directoryNode;
	}

	private @NotNull FileSystemNode createDirectory(@NotNull Path directory) {
		return nodeMap.computeIfAbsent(directory, this::newDirectory);
	}

	private boolean matchOneWay(@NotNull Path inputPath, @NotNull List<@NotNull Path> filterPaths) {
		return filterPaths.stream().anyMatch(inputPath::startsWith);
	}

	private boolean matchTwoWay(@NotNull Path inputPath, @NotNull List<@NotNull Path> filterPaths) {
		for (final Path filterPath : filterPaths) {
			if (inputPath.startsWith(filterPath) || filterPath.startsWith(inputPath)) {
				return true;
			}
		}
		return false;
	}

	private boolean matchPatterns(@NotNull String fileName, @NotNull List<@NotNull Pattern> patterns) {
		for (final Pattern pattern : patterns) {
			if (pattern.matcher(fileName).matches()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public @NotNull FileVisitResult preVisitDirectory(@NotNull Path directory,
			@NotNull BasicFileAttributes attributes) {
		if (includePaths != null && !matchTwoWay(directory, includePaths)) return FileVisitResult.SKIP_SUBTREE;
		if (excludePaths != null && matchOneWay(directory, excludePaths)) return FileVisitResult.SKIP_SUBTREE;
		final String fileName = directory.getFileName().toString();
		if (includePatterns != null && !matchPatterns(fileName, includePatterns)) return FileVisitResult.SKIP_SUBTREE;
		if (excludePatterns != null && matchPatterns(fileName, excludePatterns)) return FileVisitResult.SKIP_SUBTREE;
		createDirectory(directory);
		return FileVisitResult.CONTINUE;
	}

	@Override
	public @NotNull FileVisitResult visitFile(@NotNull Path file, @NotNull BasicFileAttributes attributes) {
		if (includePaths != null && !matchOneWay(file, includePaths)) return FileVisitResult.CONTINUE;
		if (excludePaths != null && matchOneWay(file, excludePaths)) return FileVisitResult.CONTINUE;
		final String fileName = file.getFileName().toString();
		if (includePatterns != null && !matchPatterns(fileName, includePatterns)) return FileVisitResult.SKIP_SUBTREE;
		if (excludePatterns != null && matchPatterns(fileName, excludePatterns)) return FileVisitResult.SKIP_SUBTREE;
		final FileSystemNode parentNode = createDirectory(file.getParent());
		if (attributes.isSymbolicLink()) {
			final SymbolicLinkNode linkNode = new SymbolicLinkNode();
			group.add(linkNode);
			delayedLinks.add(new Pair<>(linkNode, file));
			linkNode.setName(fileName);
			linkNode.setParent(parentNode);
			nodeMap.put(file, linkNode);
		} else if (attributes.isRegularFile()) {
			final FileNode fileNode = new FileNode();
			group.add(fileNode);
			fileNode.setName(fileName);
			fileNode.setParent(parentNode);
			nodeMap.put(file, fileNode);
		} else {
			LOGGER.info("Skipping file at \"%s\": unknown type!".formatted(file));
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public @NotNull FileVisitResult visitFileFailed(@NotNull Path file, @NotNull IOException exception)
			throws IOException {
		if (skipError) {
			LOGGER.log(Level.WARNING, "Error visiting file!", exception);
		} else {
			throw exception;
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public @NotNull FileVisitResult postVisitDirectory(@NotNull Path directory, @Nullable IOException exception)
			throws IOException {
		if (exception != null) {
			if (skipError) {
				LOGGER.log(Level.WARNING, "Error visiting directory!", exception);
			} else {
				throw exception;
			}
		}
		return FileVisitResult.CONTINUE;
	}

	private record Pair<One, Two>(@NotNull One one, @NotNull Two two) {}
}
