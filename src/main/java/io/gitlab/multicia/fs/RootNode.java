/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import io.gitlab.multicia.graph.GraphValueException;
import io.gitlab.multicia.graph.Signature;
import org.jetbrains.annotations.NotNull;

public final class RootNode extends FileSystemNode {
	/**
	 * The signature of a {@link RootNode}.
	 */
	public static final @NotNull Signature SIGNATURE = FileSystemNode.SIGNATURE.suffix("RootNode");


	/**
	 * Create a new {@link RootNode}.
	 */
	public RootNode() {
		super(SIGNATURE);
	}

	/**
	 * Create a new {@link RootNode}.
	 *
	 * @param signature The signature of the {@link RootNode}.
	 * @throws GraphValueException Throws when signature is not equals {@link #SIGNATURE}.
	 */
	public RootNode(@NotNull Signature signature) {
		super(signature);
		if (!signature.equals(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}
}
