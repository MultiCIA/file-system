/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import io.gitlab.multicia.graph.GraphValueException;
import io.gitlab.multicia.graph.Signature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class SymbolicLinkNode extends FileSystemNode {
	/**
	 * The signature of a {@link SymbolicLinkNode}.
	 */
	public static final @NotNull Signature SIGNATURE = FileSystemNode.SIGNATURE.suffix("SymbolicLinkNode");

	/**
	 * The {@link FieldHasher} for the link target in the {@link SymbolicLinkNode}.
	 */
	public static final @NotNull FieldHasher<@NotNull SymbolicLinkNode> TARGET_HASHER
			= (hasher, node) -> hasher.hash(node.getTarget());

	/**
	 * The {@link FieldMatcher} for the link target in the {@link SymbolicLinkNode}.
	 */
	public static final @NotNull FieldMatcher<@NotNull SymbolicLinkNode, @NotNull SymbolicLinkNode> TARGET_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.match(nodeA.getTarget(), nodeB.getTarget());


	/**
	 * The target {@link FileSystemNode} of this {@link SymbolicLinkNode}.
	 */
	private @Nullable FileSystemNode target = null;


	/**
	 * Create a new {@link SymbolicLinkNode}.
	 */
	public SymbolicLinkNode() {
		super(SIGNATURE);
	}

	/**
	 * Create a new {@link SymbolicLinkNode}.
	 *
	 * @param signature The signature of the {@link SymbolicLinkNode}.
	 * @throws GraphValueException Throws when signature is not equals {@link #SIGNATURE}.
	 */
	public SymbolicLinkNode(@NotNull Signature signature) {
		super(signature);
		if (!signature.equals(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}


	/**
	 * Get the target {@link FileSystemNode} of this {@link SymbolicLinkNode}.
	 *
	 * @return The target {@link FileSystemNode}.
	 */
	public @Nullable FileSystemNode getTarget() {
		return target;
	}

	/**
	 * Set the target {@link FileSystemNode} of this {@link SymbolicLinkNode}.
	 *
	 * @param target The target {@link FileSystemNode}.
	 */
	public void setTarget(@Nullable FileSystemNode target) {
		this.target = target;
	}


	@Override
	protected void writeToJson(@NotNull ObjectWriter writer) {
		super.writeToJson(writer);
		if (target != null) writer.putElement("target", target);
	}

	@Override
	protected void readFromJson(@NotNull ObjectReader reader) {
		super.readFromJson(reader);
		this.target = reader.getAsElement("target", FileSystemNode.class);
	}
}
