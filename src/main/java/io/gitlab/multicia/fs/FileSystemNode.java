/*
 * Copyright (C) 2021-2023 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.fs;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import io.gitlab.multicia.graph.GraphValueException;
import io.gitlab.multicia.graph.Signature;
import io.gitlab.multicia.graph.TreeNode;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public sealed class FileSystemNode extends TreeNode
		permits DirectoryNode, FileNode, RootNode, SymbolicLinkNode {
	/**
	 * The signature of a {@link FileSystemNode}.
	 */
	public static final @NotNull Signature SIGNATURE = TreeNode.SIGNATURE.suffix("FileSystemNode");

	/**
	 * The {@link FieldHasher} for the name in the {@link FileSystemNode}.
	 */
	public static final @NotNull FieldHasher<@NotNull FileSystemNode> NAME_HASHER
			= (hasher, node) -> Objects.hashCode(node.getName());

	/**
	 * The {@link FieldMatcher} for the name in the {@link FileSystemNode}.
	 */
	public static final @NotNull FieldMatcher<@NotNull FileSystemNode, @NotNull FileSystemNode> NAME_MATCHER
			= (matcher, nodeA, nodeB) -> Objects.equals(nodeA.getName(), nodeB.getName());


	/**
	 * The name of the {@link FileSystemNode}.
	 */
	private @Nullable String name = null;


	/**
	 * Create a new {@link FileSystemNode}.
	 *
	 * @param signature The signature of the {@link FileSystemNode}.
	 */
	public FileSystemNode(@NotNull Signature signature) {
		super(signature);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}


	/**
	 * Get the name of the {@link FileSystemNode}.
	 *
	 * @return The current name.
	 */
	public final @Nullable String getName() {
		return name;
	}

	/**
	 * Set the name of the {@link FileSystemNode}.
	 *
	 * @param name The new name.
	 */
	public final void setName(@Nullable String name) {
		this.name = name;
	}


	@MustBeInvokedByOverriders
	@Override
	protected void writeToJson(@NotNull ObjectWriter writer) {
		super.writeToJson(writer);
		if (name != null) writer.putValue("name", name);
	}

	@MustBeInvokedByOverriders
	@Override
	protected void readFromJson(@NotNull ObjectReader reader) {
		super.readFromJson(reader);
		this.name = reader.getAsString("name");
	}


	/**
	 * @return A {@link String} representation of this object.
	 */
	@Override
	public final @NotNull String toString() {
		return FileSystemHelper.toAbsolutePathString(this);
	}
}
