module io.gitlab.multicia.fs {
	requires java.logging;
	requires transitive io.gitlab.multicia.graph;
	requires static transitive org.jetbrains.annotations;

	exports io.gitlab.multicia.fs;
}